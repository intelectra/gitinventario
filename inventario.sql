DROP SCHEMA IF EXISTS inventario;
CREATE SCHEMA inventario;
USE inventario;
DROP TABLE IF EXISTS persona;
DROP TABLE IF EXISTS producto;
DROP TABLE IF EXISTS stock;
DROP TABLE IF EXISTS precios;
DROP TABLE IF EXISTS factura;
DROP TABLE IF EXISTS detalle_factura;
DROP TABLE IF EXISTS otras_salidas;
DROP TABLE IF EXISTS ubicacion_producto;
DROP TABLE IF EXISTS autenticacion;

CREATE TABLE persona(
	id_persona	integer PRIMARY KEY,
    nombre		varchar(50) NOT NULL,
    telefono	varchar(25)	NULL,
    direccion	varchar(50)	NULL,
    ciudad		varchar(25)	NULL,
    correo		varchar(50)	NULL,
    tipo		ENUM('Proveedor', 'Cliente','Propietario', 'Master','Almacenista','Surtidor','Trabajador') DEFAULT 'Cliente'
    );
    
INSERT INTO persona VALUES(66987995, "Katherine Casanova","3007813054","Capusigra","pasto","kathe@gmail.com","Cliente");
INSERT INTO persona VALUES(27502646, "Maria Urueña","3006629245","Sumapaz","buesaco","mary@gmail.com","Cliente");
INSERT INTO persona VALUES(12951953, "Julio Rosero","3017547706","Morasurco","pasto","julio@gmail.com","Cliente");
INSERT INTO persona VALUES(018999900, "Colanta","3007803054","Centro","pasto","colanta@gmail.com","Proveedor");
INSERT INTO persona VALUES(018009120, "Alpina","3007813055","Norte","cali","alpina@gmail.com","Proveedor");
INSERT INTO persona VALUES(010010001, "Bavaria","3240099663","Morasurco","pasto","bavaria@gmail.com","Proveedor");
INSERT INTO persona VALUES(012001023, "Zenu","3007772055","Melendez","cali","zenu@gmail.com","Proveedor");
INSERT INTO persona VALUES(018009123, "Postobon","3247813055","Normandia","bogota","zenu@gmail.com","Proveedor");
INSERT INTO persona VALUES(12955963, "Cesar Rosero","3006149734","La Estancia","pasto","cesarosero@gmail.com","Propietario");
INSERT INTO persona VALUES(20000000, "Alejandro Gallego","3178975423","Cedritos","bogota","jagallego@gmail.com","Master");
INSERT INTO persona VALUES(30000000, "Beimer Ortega","3152030406","Poblado","buenaventura","beimeralmaceno@gmail.com","Almacenista");
INSERT INTO persona VALUES(40000000, "Andres Vega","3113404064","Candelilla","riosucio","vegaspro@gmail.com","Surtidor");
INSERT INTO persona VALUES(50000000, "Alejandro Montero","3221344581","Prado Norte","bogota","amontero@gmail.com","Trabajador");
INSERT INTO persona VALUES(2899818,"GERARDO ANDRADE GOMEZ","3202861920","Los Pinos","medellin","crisaliatorborojo@yahoo.es","Cliente");
INSERT INTO persona VALUES(1911119,"JOSE DEL CARMEN IBARRA","3108520664","La San Juana","cali","Asojuveara.cali@gmail.com","Cliente");
INSERT INTO persona VALUES(1937809,"JOSE DEL CARMEN ARIAS PEREZ","3162871998","Santa Rita ","cali","mariahv71@yahoo.com","Cliente");
INSERT INTO persona VALUES(1938067,"ALBINO ANTONIO RODRIGUEZ GAONA","3218730425","FrancoVilla","ipiales","rubiquy@yahoo.es","Cliente");
INSERT INTO persona VALUES(1938203,"ALEJO  VARGAS MARTINEZ","3136066742","El Alto ","bogota ","cide183@gmail.com","Cliente");
INSERT INTO persona VALUES(1938213,"FRANCISCO  ORTEGA AGUDELO ","3137551572","Naranjal","bogota","miguelcassiani@hotmail.com","Cliente");
INSERT INTO persona VALUES(1938223,"JOSE DEL CARMEN YARURO SEPULVEDA ","3188753268","El Helecha","bogota","milenormosquera@hotmail.com","Cliente");
INSERT INTO persona VALUES(1938229,"PABLO ANTONIO ARIAS PEREZ","3106657357","San Pablo ","cartago","vipibe1952@hotmail.com","Cliente");
INSERT INTO persona VALUES(1938259,"ROSO ARIAS PEREZ","3108804171","El Alto ","cartagena","genadi76@gmail.com","Cliente");
INSERT INTO persona VALUES(1938284,"EUTIMIO PEDRAZA GAONA","3173473480","Bucarasica","cartagena","artotumosltda@gmail.com","Cliente");
INSERT INTO persona VALUES(1938289,"RAMON CELIS VARGAS","3013614948","Balcones ","Mosquera","luisangulomosquera@yahoo.es","Cliente");
INSERT INTO persona VALUES(1938317,"FRANCISCO VARGAS HEREDIA","3205697711","Balcones ","cartago","tiaretkimya@hotmail.com","Cliente");
INSERT INTO persona VALUES(1938331,"HUMBERTO ORTIZ RAMIREZ","3207694674","La Gamuza ","medellin","waltervides50@hotmail.com","Cliente");
INSERT INTO persona VALUES(1938335,"FRANCISCO ALBERTO GALLO MARQUEZ","3175003040","La San Juana ","cartago","jofenosgon@yahoo.com","Cliente");
INSERT INTO persona VALUES(1938377,"REMIGIO QUINTERO LOPEZ","3142082402","Urbano ","cartago","roselhimen2@hotmail.com","Cliente");
INSERT INTO persona VALUES(1938432,"OTILIO SERRANO CASTELLANOS","3114575330","Filo Seco ","cartago","copete.maximo5@gmail.com","Cliente");
INSERT INTO persona VALUES(1938496,"PEDRO EMILIO TAMARA ARIAS","3172778419","El Alto ","buesaco","mangelerenteria@gmail.com","Cliente");
INSERT INTO persona VALUES(1938511,"LEUTO QUINTERO LOPEZ","3168926542","Bucarasica","buesaco","oeacor@gmail.com","Cliente");
INSERT INTO persona VALUES(1938522,"NICOLAS SANCHEZ RIOS","3178689518","Centro","buesaco","cdesamo@yahoo.com","Cliente");
INSERT INTO persona VALUES(1938557,"SEBASTIAN ARIAS QUINTERO","3215261009","El Carmen ","buesaco","edwinguerrero0828@hotmail.com","Cliente");
INSERT INTO persona VALUES(176307332,"WILLIAM EFRAIN ABELLA HERRERA ","3126116204","Centro","samaniego","yusona@hotmail.es","Cliente");
INSERT INTO persona VALUES(210547808,"FAUSTO JOSE ACOSTA","3137823674","Centro","samaniego","diyulieth1999@gmail.com ","Cliente");
INSERT INTO persona VALUES(310516932,"PEREGRINO  ACOSTA ","3146301864","Centro","samaniego","faridespitre@yahoo.com","Cliente");
INSERT INTO persona VALUES(434532270,"MARIA AMPARO ACOSTA ARAGON","3117236128","Centro","samaniego","denns18@hotmail.com","Cliente");
INSERT INTO persona VALUES(576323459,"GUEFRY LEIDER AGREDO MENDEZ ","3146034723","Obrero","samaniego","caralcuba@hotmail.com","Cliente");
INSERT INTO persona VALUES(634531725,"XIMENA LUCIA AGREDO TOBAR ","3114221715","Obrero","samaniego","afrohuila@yahoo.com","Cliente");
INSERT INTO persona VALUES(1087630572,"GUILLERMO ALEJANDRO AGREDO TORRES ","3126242446","Obrero","samaniego","kumkumbamana@hotmail.com","Cliente");
INSERT INTO persona VALUES(1088487056,"NORA ELENAAGUDELO DE LOPEZ","3123066509","La Loma","buesaco","alejandrotorres@gmail.com","Cliente");
INSERT INTO persona VALUES(105076432,"JUAN CARLOS AGUIRRE GARCIA","3143400301","La Loma","buesaco","juancarlosagui@gmail.com","Cliente");
INSERT INTO persona VALUES(1086153763,"NOE ALBAN LOPEZ ","3004228232","San Fernando","buesaco","labarcadenode@gmail.com","Cliente");
INSERT INTO persona VALUES(1087628830,"DIEGO ALONSO ALEGRIA FERNANDEZ ","3152374841","San Fernando","buesaco","diegoalegria@hotmail.com","Cliente");
INSERT INTO persona VALUES(1087631155,"CARLOS ALBERTO ALEGRIA VELASQUEZ ","3168936540","La estancia","buesaco","cabetoalegron@gmail.com","Cliente");
INSERT INTO persona VALUES(105366365,"LUIS EVELIO  ALVAREZ JARAMILLO ","3188682456","La estancia","buesaco","luievelioalva@hotmail.com","Cliente");
INSERT INTO persona VALUES(41733718,"MARIA CECILIA ALVAREZ VEJARANO","3125367843","Villa Sol","buesaco","chiaseximarie@yahoo.es","Cliente");
INSERT INTO persona VALUES(274613691,"LUIS GERARDO ALVEAR ORTEGA ","3136748234","Villa Sol","buesaco","luisalvear@hotmail.es","Cliente");
INSERT INTO persona VALUES(104538776,"MARY LUZ ALZATE CAMAYO ","3166301864","Villa Sol","pasto","alzateyyo@gmail.com","Cliente");
INSERT INTO persona VALUES(10545657,"LUIS ALFONSO ANAYA CABRERA","3173112361","Veracruz","pasto","alfonsoanaya@gmail.com","Cliente");
INSERT INTO persona VALUES(10539634,"CARLOS ALBERTO ANAYA HIDALGO ","3144224723","Veracruz","pasto","cabezonabayn@gmail.com","Cliente");
INSERT INTO persona VALUES(34528262,"LUZ AMPARO ANDRADE DE VALENCIA","3043236754","Veracruz","cartago","amparoDose@gmail.com","Cliente");
INSERT INTO persona VALUES(77744455,"JORGE ANDRES VEGA ESPAÑOL","3137675898","Villa santos","pererira","asdalkdhlaksd@gmail.com","Cliente");
INSERT INTO persona VALUES(55526855,"JUAN MANUEL SANTOS ","3214587895","Villa santos","pererira","asafdsffsdf@gmail.com	","Cliente");


CREATE TABLE producto(
	id_producto				integer auto_increment PRIMARY KEY,
    codigo_producto			varchar(50) UNIQUE NULL,
    nombre_producto			varchar(50)	NOT NULL,
    marca_producto			varchar(20) NULL,
    descripcion_producto	varchar(30)	NULL
    );
   
INSERT INTO producto VALUES(1,"7700001001","Leche Entera","Alpina","Bolsa 1 litro");
INSERT INTO producto VALUES(2,"7700001002","Leche Deslactosada","Alpina","Bolsa 1 litro");
INSERT INTO producto VALUES(3,"7700001003","Leche Deslactosada","Alpina","Caja 1 litro");
INSERT INTO producto VALUES(4,"7700001004","Yogourt Griego Fresa","Alpina","Sixpack");
INSERT INTO producto VALUES(5,"7700002001","Leche Entera","Colanta","Bolsa 1 litro");
INSERT INTO producto VALUES(6,"7700002002","Leche Deslactosada","Colanta","Bolsa 1 litro");
INSERT INTO producto VALUES(7,"7700002003","Leche Deslactosada","Colanta","Caja 1 litro");
INSERT INTO producto VALUES(8,"7700003001","Cerveza Poker","Poker","350ml");
INSERT INTO producto VALUES(9,"7700003002","Cerveza Pokeron","Poker","750ml");
INSERT INTO producto VALUES(10,"7700003003","Cerveza Pura Malta","Poker","350ml");
INSERT INTO producto VALUES(11,"7700003004","Cerveza Poker Roja","Poker","350ml");
INSERT INTO producto VALUES(12,"7700003101","Cerveza Club Colombia Dorada","Club Colombia","350ml");
INSERT INTO producto VALUES(13,"7700003102","Cerveza Club Colombia Roja","Club Colombia","350ml");
INSERT INTO producto VALUES(14,"7700003103","Cerveza Club Colombia Negra","Club Colombia","350ml");
INSERT INTO producto VALUES(15,"7700003104","Cerveza Club Colombia Doble Malta","Club Colombia","350ml");
INSERT INTO producto VALUES(16,"7700004101","Salchichas Ranchera","Zenu","Paquete por 10 u");
INSERT INTO producto VALUES(17,"7700004002","Costillas Ahumadas","Zenu","Paquete por 500 g");
INSERT INTO producto VALUES(18,"7700004103","Salchichas Zenu","Zenu","Tarro 250 g");
INSERT INTO producto VALUES(19,"7700005001","Gaseosa Manzana Postobon","Postobon","1 litro");
INSERT INTO producto VALUES(20,"7700005002","Gaseosa Pepsi","Postobon","1 litro");

CREATE TABLE stock(
	id_producto 	integer NOT NULL,
    stock_maximo	integer	NULL,
    stock_minimo	integer	NOT NULL,
    FOREIGN KEY(id_producto)REFERENCES producto(id_producto)
    );
INSERT INTO stock VALUES(1, 250, 40);
INSERT INTO stock VALUES(2, 120, 30);
INSERT INTO stock VALUES(3, 100, 25);
INSERT INTO stock VALUES(4, 90, 20);
INSERT INTO stock VALUES(5, 300, 45);
INSERT INTO stock VALUES(6, 250, 40);
   
CREATE TABLE precios(
	id_producto 	integer NOT NULL,
    precio_compra 	double NOT NULL,
    precio_venta	double NOT NULL,
    FOREIGN KEY(id_producto)REFERENCES producto(id_producto)
    );

INSERT INTO precios VALUES(1, 2000, 3000);
INSERT INTO precios VALUES(2, 3000, 4000);
INSERT INTO precios VALUES(3, 4000, 6000);
INSERT INTO precios VALUES(4, 5000, 8000);
INSERT INTO precios VALUES(5, 1500, 2500);
INSERT INTO precios VALUES(6, 2000, 3000);
INSERT INTO precios VALUES(7, 3000, 4000);
INSERT INTO precios VALUES(8, 1200, 2500);
INSERT INTO precios VALUES(9, 2500, 4000);
INSERT INTO precios VALUES(10, 1500, 3000);
INSERT INTO precios VALUES(11, 1500, 3000);
INSERT INTO precios VALUES(12, 1800, 3500);
INSERT INTO precios VALUES(13, 1800, 3500);
INSERT INTO precios VALUES(14, 2000, 3800);
INSERT INTO precios VALUES(15, 2500, 4000);
INSERT INTO precios VALUES(16, 12000, 15000);
    
CREATE TABLE factura(
	id_factura		integer auto_increment PRIMARY KEY,
    id_persona		integer	NOT NULL,
    valor_total		double,
    fecha			datetime,
    tipo			ENUM('Compra', 'Venta'),
    FOREIGN KEY(id_persona)REFERENCES persona(id_persona)
    );
   
INSERT INTO factura VALUES(1,66987995,28000,'2021-09-01 20:00:00','Venta');
INSERT INTO factura VALUES(2,27502646,27000,'2021-09-01 20:20:00','Venta');
INSERT INTO factura VALUES(3,12951953,40000,'2021-09-01 20:40:00','Venta');
INSERT INTO factura VALUES(4,018999900,400000,'2021-09-02 8:00:00','Compra');
INSERT INTO factura VALUES(5,018009123,360000,'2021-09-03 13:00:00','Compra');

CREATE TABLE detalle_factura(
	id_factura		integer NOT NULL,
    id_producto		integer NOT NULL,
    cantidad		integer NOT NULL,
    FOREIGN KEY(id_factura)REFERENCES factura(id_factura),
    FOREIGN KEY(id_producto)REFERENCES producto(id_producto)
    );

INSERT INTO detalle_factura VALUES(1,2,3);
INSERT INTO detalle_factura VALUES(1,15,4);
INSERT INTO detalle_factura VALUES(2,3,2);
INSERT INTO detalle_factura VALUES(2,16,1);
INSERT INTO detalle_factura VALUES(3,4,8);
INSERT INTO detalle_factura VALUES(4,1,200);
INSERT INTO detalle_factura VALUES(4,2,120);

CREATE TABLE otras_salidas(
	id_producto		integer NOT NULL,
    descripcion		varchar(50),
    cantidad		smallint,
    FOREIGN KEY(id_producto)REFERENCES producto(id_producto)
    );
    
INSERT INTO otras_salidas VALUES(1, "Vencimiento",2);
INSERT INTO otras_salidas VALUES(2, "Vencimiento",3);
INSERT INTO otras_salidas VALUES(5, "Vencimiento",2);

CREATE TABLE ubicacion_producto(
	id_producto 	integer	NOT NULL,
    ubicacion		varchar(20) NOT NULL,
    cantidad		integer NULL,
    foreign key(id_producto)REFERENCES producto(id_producto)
    );
    
INSERT INTO ubicacion_producto VALUES(1, "Refrigerador 1", 10);
INSERT INTO ubicacion_producto VALUES(1, "Bodega", 190);
INSERT INTO ubicacion_producto VALUES(2, "Refrigerador 1", 10);
INSERT INTO ubicacion_producto VALUES(2, "Bodega", 110);
INSERT INTO ubicacion_producto VALUES(3, "Refrigerador 1", 10);
INSERT INTO ubicacion_producto VALUES(3, "Bodega", 190);
INSERT INTO ubicacion_producto VALUES(4, "Refrigerador 1", 10);
INSERT INTO ubicacion_producto VALUES(4, "Bodega", 140);

CREATE TABLE autenticacion(
    id_persona 	integer	NOT NULL,
    rol			varchar(15),
    clave		varchar(15),
    FOREIGN KEY(id_persona)REFERENCES persona(id_persona)
);
INSERT INTO autenticacion VALUES(12955963,"Propietario","Propietario123");
INSERT INTO autenticacion VALUES(20000000,"Master","Master123");
INSERT INTO autenticacion VALUES(30000000,"Almacenista","Almacen123");
INSERT INTO autenticacion VALUES(40000000,"Surtidor","Susrtidor123");
INSERT INTO autenticacion VALUES(50000000,"Trabajador","Traba123");